#ifndef HTTP_DEFLATE_HPP
#define HTTP_DEFLATE_HPP

/**
 * @file Deflate.hpp
 *
 * This module declares the Http::Deflate function and its friends.
 * 
 */

#include <stdint.h>
#include <vector>
#include <string>

namespace Http {

    /**
     *  This is used to pick a compression mode for the Deflate method
     */
    enum class DeflateMode {

        Deflate,

        Gzip,

    };

    std::vector< uint8_t > Deflate(
        const std::vector< uint8_t >& input,
        DeflateMode mode
    );

    std::string Deflate(
        const std::string& input,
        DeflateMode mode
    );

}

#endif