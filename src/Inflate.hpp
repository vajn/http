#ifndef HTTP_INFLATE_HPP
#define HTTP_INFLATE_HPP

/**
 * @file Inflate.hpp
 *
 * This module declares the Http::Inflate function and its friends.
 *
 */

#include <stdint.h>
#include <string>
#include <vector>

namespace Http {

    enum class InflateMode {

        /**
         *  This selects the "zlib" data format
         */
        Inflate,

        /**
         *  This select the "gzip" data format
         */
        Ungzip,

    };

    /**
     * This function decompresses the given vector of bytes, using
     * the given decomprerssion mode
     */
    bool Inflate(
        const std::vector< uint8_t >& input,
        std::vector< uint8_t >& output,
        InflateMode mode
    );

    /**
     * This function decompresses the given string, using
     * the given decomprerssion mode
     */
    bool Inflate(
        const std::string& input,
        std::string& output,
        InflateMode mode
    ); 

}

#endif