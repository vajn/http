#ifndef HTTP_TIME_KEEPER_HPP
#define HTTP_TIME_KEEPER_HPP

/**
 * @file TimeKeeper.hpp
 *
 * This module declares the Http::TimeKeeper interface.
 */
#include "Connection.hpp"

#include <functional>
#include <memory>
#include <stdint.h>

namespace Http {

    /**
     *  维护目前真实的服务器时间状态
     */ 
    class TimeKeeper {
    public:
        virtual double GetCurrentTime() = 0;
    };
    
}

#endif