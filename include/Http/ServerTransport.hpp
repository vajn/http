#ifndef HTTP_SERVER_TRANSPORT_HPP
#define HTTP_SERVER_TRANSPORT_HPP

/**
 * @file ServerTransport.hpp
 *
 * This module declares the Http::ServerTransport interface.
 *
 */

#include "Connection.hpp"

#include <functional>
#include <memory>
#include <stdint.h>

namespace Http {

    class ServerTransport {
    public:

    /**
     *  连接准备好的回调
     */
    typedef std::function< void() > ConnectionReadyDelegate;

    /**
     *  客户端与服务器连接建立好后的回调
     */
    typedef std::function<
        ConnectionReadyDelegate(std::shared_ptr< Connection > connection)
    > NewConnectionDelegate;

    /**
     *  绑定网络接口
     */
    virtual bool BindNetwork(
        uint16_t port,
        NewConnectionDelegate newConnectionDelegate
    ) = 0;

    /**
     *  获取当前绑定的端口
     */
    virtual uint16_t GetBoundPort() = 0;

    /**
     *  释放网络
     */
    virtual void ReleaseNetwork() = 0;

    };

}

#endif