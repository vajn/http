#ifndef HTTP_REQUEST_HPP
#define HTTP_REQUEST_HPP

/**
 * @file Request.hpp
 *
 * This module declares the Http::Request structure.
 *
 */

#include <MessageHeaders/MessageHeaders.hpp>
#include <ostream>
#include <string>
#include <Uri/Uri.hpp>

namespace Http
{
    /**
     *  一个 HTTP 请求的结构
     */
    struct Request {

        /**
         *  目前请求构造的状态
         */
        enum class State {

            RequestLine,

            Headers,

            Body,

            Complete,

            Error,

        };

        /**
         *  请求是否合法
         */
        bool valid = true;

        /**
         *  记录构造请求的总字节数
         */
        size_t totalBytes = 0;

        /**
         *  请求方法
         */
        std::string method;

        /**
         *  请求资源的 Uri
         */
        Uri::Uri target;

        /**
         *  请求头
         */
        MessageHeaders::MessageHeaders headers;

        /**
         *  请求体
         */
        std::string body;

        /**
         *  状态初始化，初始状态为解析请求行
         */
        State state = State::RequestLine;

        /**
         *  响应码
         */
        unsigned int responseStatusCode = 400;

        std::string responseReasonPhrase = "Bad Request";

        /**
         *  是否解析完成
         */
        bool IsCompleteOrError() const;

        /**
         * 请求内容生成字符串
         */
        std::string Generate() const;
    };

    void PrintTo(
        const Request::State& state,
        std::ostream* os
    );
    
} // namespace Http



#endif