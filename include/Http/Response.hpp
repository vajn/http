#ifndef HTTP_RESPONSE_HPP
#define HTTP_RESPONSE_HPP

/**
 * @file Response.hpp
 *
 * This module declares the Http::Response structure.
 *
 */

#include <MessageHeaders/MessageHeaders.hpp>
#include <string>

namespace Http {

    struct Response {

        /**
         *  响应状态
         */
        enum class State {

            StatusLine,

            Headers,

            Body,

            Complete,

            Error,

        };

        bool valid = true;

        /**
         *  状态码
         */
        unsigned int statusCode;

        std::string reasonPhrase;

        MessageHeaders::MessageHeaders headers;

        std::string body;

        State state = State::StatusLine;

        bool IsCompleteOrError(bool moreDataPossible = true) const;

        std::string Generate() const;
    };

}

#endif /* HTTP_RESPONSE_HPP */