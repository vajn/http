#ifndef HTTP_CHUNKED_BODY_HPP
#define HTTP_CHUNKED_BODY_HPP

/**
 * @file ChunkedBody.hpp
 *
 * This module declares the Http::ChunkedBody class.
 * 
 */

#include <MessageHeaders/MessageHeaders.hpp>
#include <memory>
#include <string>

namespace Http {

    class ChunkedBody {

    public:

        enum class State {

            DecodingChunks,

            ReadingChunkData,

            ReadingChunkDelimiter,

            DecodingTrailer,

            Complete,

            Error,

        };

    // 生命周期管理
    public:
        ~ChunkedBody() noexcept;
        ChunkedBody(const ChunkedBody&) = delete;
        ChunkedBody(ChunkedBody&&) noexcept;
        ChunkedBody& operator=(const ChunkedBody&) = delete;
        ChunkedBody& operator=(ChunkedBody&&) noexcept;

    public:

        // 默认构造函数
        ChunkedBody();

        /**
         *  解码 Chuncked body
         */
        size_t Decode(
            const std::string& input,
            size_t offset = 0,
            size_t length = 0
        );

        State GetState() const;

        /**
         * 将 body 以字符串形式返回
         */
        operator std::string() const;


        const MessageHeaders::MessageHeaders& GetTrailers() const;

    private:

        struct Impl;

        std::unique_ptr< Impl > impl_;

    };

    void PrintTo(
        const Http::ChunkedBody::State& state,
        std::ostream* os
    );

}


#endif