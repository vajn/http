#ifndef HTTP_CLIENT_TRANSPORT_HPP
#define HTTP_CLIENT_TRANSPORT_HPP

/**
 * @file ClientTransport.hpp
 *
 * This module declares the Http::ClientTransport interface.
 *
 */

#include "Connection.hpp"

#include <memory>
#include <stdint.h>
#include <string>

namespace Http {

    /**
     *  This represents the transport layer requirements of HTTP::Client
     */
    class ClientTransport {
    public:

        /**
         *  This method establishes a new connection to a server with the given
         *  address and port number
         */
        virtual std::shared_ptr< Connection > Connect(
            const std::string& scheme,
            const std::string& hostNameOrAddress,
            uint16_t port,
            Http::Connection::DataReceivedDelegate dataReceivedDelegate,
            Http::Connection::BrokenDelegate brokenDelegate
        ) = 0;

    };
    

}


#endif